package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type collector struct {
	t      *http.Transport
	apiKey string
}

func (c *collector) request(reqChan chan *http.Request, start, end time.Time) {
	defer close(reqChan)
	for start.Before(end) {
		req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("https://api.nasa.gov/planetary/apod?api_key=%s&date=%s", c.apiKey, start.Format("2006-01-02")), nil)
		if err != nil {
			log.Println(err)
		}
		reqChan <- req
		start = start.Add(24 * time.Hour)
	}
}

func (c *collector) do(reqChan chan *http.Request, respChan chan Response) {
	for req := range reqChan {
		resp, err := c.t.RoundTrip(req)
		r := Response{resp, err}
		respChan <- r
	}
}

func (c *collector) getPictures(concurrentReq int, reqChan chan *http.Request, respChan chan Response) {
	for i := 0; i < concurrentReq; i++ {
		go c.do(reqChan, respChan)
	}
}

func (c *collector) readData(maxReq int64, respChan chan Response) (pictures []Picture) {
	var conns int64
	for conns < maxReq {
		r, ok := <-respChan
		if ok {
			if r.err != nil {
				log.Println(r.err)
			} else {
				data, err := ioutil.ReadAll(r.Body)
				if err != nil {
					log.Println(r.err)
					continue
				}
				p := Picture{}
				if err := json.Unmarshal(data, &p); err != nil {
					log.Print("unable to decode picture data")
				} else {
					pictures = append(pictures, p)
				}
				if err := r.Body.Close(); err != nil {
					log.Println(r.err)
				}
				conns++
			}
		}
	}
	return
}
