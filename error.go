package main

import "bytes"

type Error struct {
	HttpStatusCode int
	Message        string
	Err            error
}

func (e *Error) Error() string {
	var buf bytes.Buffer
	buf.WriteString(e.Message)
	if e.Err != nil {
		buf.WriteString(" error:")
		buf.WriteString(e.Err.Error())
	}
	return buf.String()
}

func (e *Error) Unwrap() error {
	return e.Err
}

func (e *Error) Is(target error) bool {
	t, ok := target.(*Error)
	if !ok {
		return false
	}
	return (e.Message == t.Message || t.Message == "") &&
		(e.HttpStatusCode == t.HttpStatusCode || t.HttpStatusCode == 0)
}
