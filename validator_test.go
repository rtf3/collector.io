package main

import (
	"net/http"
	"testing"
	"time"
)

func Test_validator_Pictures(t *testing.T) {
	type fields struct {
		collector         *collector
		reqChan           chan *http.Request
		respChan          chan Response
		concurrentRequest int
	}
	type args struct {
		reqChan  chan *http.Request
		strStart string
		strEnd   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "invalid start date provided",
			fields: fields{
				collector: &collector{
					apiKey: "test_api_key",
					t:      &http.Transport{},
				},
				reqChan:           make(chan *http.Request),
				respChan:          make(chan Response),
				concurrentRequest: 1,
			},
			args: args{
				strStart: "invalid date",
				strEnd:   time.Now().Format("2006-01-02"),
			},
			wantErr: true,
		}, {
			name: "invalid end date provided",
			fields: fields{
				collector: &collector{
					apiKey: "test_api_key",
					t:      &http.Transport{},
				},
				reqChan:           make(chan *http.Request),
				respChan:          make(chan Response),
				concurrentRequest: 2,
			},
			args: args{
				reqChan:  make(chan *http.Request),
				strStart: time.Now().Add(-24 * time.Hour).Format("2006-01-02"),
				strEnd:   "invalid end date",
			},
			wantErr: true,
		}, {
			name: "invalida date range provided",
			fields: fields{
				collector: &collector{
					apiKey: "test_api_key",
					t:      &http.Transport{},
				},
				reqChan:           make(chan *http.Request),
				respChan:          make(chan Response),
				concurrentRequest: 3,
			},
			args: args{
				reqChan:  make(chan *http.Request),
				strEnd:   time.Now().Add(-24 * time.Hour).Format("2006-01-02"),
				strStart: time.Now().Format("2006-01-02"),
			},
			wantErr: true,
		}, {
			name: "create request",
			fields: fields{
				collector: &collector{
					apiKey: "test_api_key",
					t:      &http.Transport{},
				},
				reqChan:           make(chan *http.Request),
				respChan:          make(chan Response),
				concurrentRequest: 4,
			},
			args: args{
				reqChan:  make(chan *http.Request),
				strStart: time.Now().Add(-24 * time.Hour).Format("2006-01-02"),
				strEnd:   time.Now().Format("2006-01-02"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer close(tt.fields.reqChan)
			v := &validator{
				collector:      tt.fields.collector,
				reqChan:        tt.fields.reqChan,
				respChan:       tt.fields.respChan,
				concurrRequest: tt.fields.concurrentRequest,
			}
			if _, err := v.Pictures(tt.args.strStart, tt.args.strEnd); (err != nil) != tt.wantErr {
				t.Errorf("validator.request() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
