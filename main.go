package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

const (
	ENV_API_KEY             = "API_KEY"
	ENV_CONCURRENCT_REQUEST = "CONCURRENT_REQUESTS"
	ENV_PORT                = "PORT"
)

var (
	apiKey        string
	concurrentReq int = 1
	port          int = 8082
	present       bool
	err           error
)

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	apiKey, present = os.LookupEnv(ENV_API_KEY)
	if !present {
		apiKey = "DEMO_KEY"
	}
	countConcurr, present := os.LookupEnv(ENV_CONCURRENCT_REQUEST)
	if present {
		concurrentReq, err = strconv.Atoi(countConcurr)
		if err != nil {
			panic("invalid concurrency request count provided")
		}
	}

	p, present := os.LookupEnv(ENV_PORT)
	if present {
		port, err = strconv.Atoi(p)
		if err != nil {
			panic(fmt.Sprintf("invlid port provided: %v", err))
		}
	}

	srv := New(apiKey, concurrentReq)

	handler := NewHandler(srv, mux.NewRouter())

	server := &http.Server{
		Handler:      handler.router,
		Addr:         fmt.Sprintf(":%d", port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(server.ListenAndServe())
}
