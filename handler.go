package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type handler struct {
	collector Service
	router    *mux.Router
}

func NewHandler(collector Service, router *mux.Router) *handler {
	h := &handler{
		collector: collector,
	}
	router.HandleFunc("/pictures", h.pictures)
	h.router = router
	return h
}

func (h *handler) pictures(w http.ResponseWriter, r *http.Request) {
	startDate := r.URL.Query().Get("start_date")
	endDate := r.URL.Query().Get("end_date")
	p, err := h.collector.Pictures(startDate, endDate)
	if err != nil {
		if errors.Is(err, &Error{}) {
			w.WriteHeader(err.(*Error).HttpStatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Errorf("an unexpected error has happened:%w", err).Error()))
		}
		return
	}
	type response struct {
		Data []Picture `json:"url"`
	}
	rsp := response{p}
	data, err := json.Marshal(rsp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		e := &Error{Message: "unable to encode to json"}
		w.Write([]byte(e.Error()))
	}
	w.Write(data)
}
