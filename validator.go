package main

import (
	"fmt"
	"net/http"
	"time"
)

type validator struct {
	*collector
	reqChan        chan *http.Request
	respChan       chan Response
	concurrRequest int
}

func New(apiKey string, concurrencyRequest int) *validator {
	return &validator{
		collector: &collector{
			apiKey: apiKey,
			t:      &http.Transport{},
		},
		reqChan:        make(chan *http.Request),
		respChan:       make(chan Response),
		concurrRequest: concurrencyRequest,
	}
}

func (v *validator) Pictures(strStart, strEnd string) ([]Picture, error) {
	start, err := time.Parse("2006-01-02", strStart)
	if err != nil {
		return nil, fmt.Errorf("invalid start date provided: %w", &Error{
			HttpStatusCode: http.StatusBadRequest,
			Message:        "invalid start date",
		})
	}
	end, err := time.Parse("2006-01-02", strEnd)
	if err != nil {
		return nil, fmt.Errorf("invalid end date provided: %w", &Error{
			HttpStatusCode: http.StatusBadRequest,
			Message:        "invalid end date",
		})
	}
	if !start.Before(end) {
		return nil, fmt.Errorf("wrong start date range provided: %w", &Error{
			HttpStatusCode: http.StatusBadRequest,
			Message:        "start date must be before end date",
		})
	}
	maxRequest := int64(end.Sub(start).Hours() / 24)
	reqChan := make(chan *http.Request)
	respChan := make(chan Response)

	defer close(respChan)

	go v.request(reqChan, start, end)
	go v.getPictures(v.concurrRequest, reqChan, respChan)
	pictures := v.readData(maxRequest, respChan)

	return pictures, nil
}
