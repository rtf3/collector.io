package main

type Service interface {
	Pictures(start, end string) ([]Picture, error)
}
