package main

import "net/http"

type Picture struct {
	Date           string `json:"date"`
	Title          string `json:"title"`
	Explanation    string `json:"explanation"`
	HDurl          string `json:"hdurl"`
	ServiceVersion string `json:"service_version"`
	MediaType      string `json:"media_type"`
	URL            string `json:"url"`
	Public         bool   `json:"public"`
}

type Response struct {
	*http.Response
	err error
}
